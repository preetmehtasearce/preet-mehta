connection: "inhouse_bi"

# include all the views
include: "/views/**/*.view"

datagroup: delete_recover_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: delete_recover_default_datagroup

explore: comparison_covid_era {}

# explore: covid_depression {}

explore: looker_user_billing_data {}

explore: faostat {}

explore: us_accidents {}

# explore: country_trial {}
