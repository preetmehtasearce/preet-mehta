view: comparison_covid_era {
  sql_table_name: `Inhouse_BI.comparison_covid_era`
    ;;

  dimension: country {
    type: string
    map_layer_name: countries
    sql: ${TABLE}.Country ;;
  }

  dimension: depression {
    type: number
    sql: ${TABLE}.Depression ;;
  }

  dimension: year {
    type: string
    sql: ${TABLE}.Year ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
