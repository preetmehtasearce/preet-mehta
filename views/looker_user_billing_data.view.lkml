view: looker_user_billing_data {
  sql_table_name: `Inhouse_BI.looker_user_billing_data`
    ;;

  dimension: gcp_service_account {
    type: string
    sql: ${TABLE}.gcp_service_account ;;
  }

  dimension: looker_user_email {
    type: string
    sql: ${TABLE}.looker_user_email ;;
  }

  dimension: looker_user_id {
    type: string
    sql: ${TABLE}.looker_user_id ;;
  }

  dimension: total_gbs_billed {
    type: number
    sql: ${TABLE}.total_gbs_billed ;;
  }

  dimension: total_gbs_processed {
    type: number
    sql: ${TABLE}.total_gbs_processed ;;
  }

  measure: count {
    type: count
    drill_fields: []
  }
}
